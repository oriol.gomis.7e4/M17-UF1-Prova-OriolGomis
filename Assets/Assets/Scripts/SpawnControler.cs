﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnControler : MonoBehaviour
{
    [SerializeField]
    private GameObject[] enemy;

    private float enem;

    public Vector2 randomX;
    public Vector2 randomY;
    // Start is called before the first frame update
    void Start()
    {
        
        
        InvokeRepeating("CreateEnemy",1,1.5f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void CreateEnemy() {

        enem = Random.Range(0,enemy.Length);
        int num = (int)enem;
        Instantiate(enemy[num], new Vector3(Random.RandomRange(randomX.x, randomX.y), Random.RandomRange(randomY.x, randomY.y), 0), Quaternion.identity);
       
    }
}
