﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private int dmg = 1;
    public UiController score;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += new Vector3(0,0.1f);
        if (transform.position.y>=5f) {
            Destroy(this.gameObject);
        }
    }


    private void OnTriggerEnter2D(Collider2D collision) {
        EnemyDeath enemy = collision.GetComponent<EnemyDeath>();
        if (enabled != null)
        {
            enemy.TakeDamage(dmg);
        }
        score.punt += 5;
        Destroy(this.gameObject);
        
    }
}

