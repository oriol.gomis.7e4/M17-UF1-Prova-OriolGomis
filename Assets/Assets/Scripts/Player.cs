﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public int score;
    private Vector3 Playerpos;
    public GameObject bulletprefab;
    // Start is called before the first frame update
    void Start()
    {
        Playerpos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.position += new Vector3(0, 0.05f);
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.position += new Vector3(0, -0.05f);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.position += new Vector3(-0.05f, 0);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.position += new Vector3(0.05f, 0);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {

            Shoot();

        }




    }
    void Shoot()
    {


        Instantiate(bulletprefab, new Vector3(transform.position.x, (transform.position.y)+1), transform.rotation);
        if (bulletprefab.transform.position.y>=5f) {
            Destroy(bulletprefab);
        }




    }

}
